# Monkey Interpreter

An interpreter for the [Monkey](https://monkeylang.org/) language written in Go.  
Following the book [Writing an Interpreter in Go](https://interpreterbook.com/) by Thorsten Ball

## Usage

- To run REPL (Read Eval Print Loop)

```shell
go run main.go
```

- To build

```shell
go build

# then run executable
./monkey
```

- To run tests (on all subpackages)

```shell
 go test ./...
```

- To format files (on all subpackages)

```shell
go fmt ./...
```